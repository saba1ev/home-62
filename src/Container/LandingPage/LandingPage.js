import React, {Component, Fragment} from 'react';

import './LandingPage.css'
class LandingPage extends Component {
  render() {
    return (
      <Fragment>
        <div className='Bg-header'>
          <div className='Opacity'>
            <header className='Header'>
              <div className="container">
                <div className="logo">
                  <a href="/"/>
                </div>
                <nav>
                  <ul className='Ul-nav'>
                    <li><a href="/">Home</a></li>
                    <li><a href={"/AboutUs"}>About us</a></li>
                    <li><a href="#">Contact</a></li>
                  </ul>
                </nav>
              </div>
            </header>
            <div className='container'>
              <h1 className='Title'>Сonstruction company</h1>
            </div>
          </div>
        </div>
      </Fragment> 
    );
  }
}

export default LandingPage;